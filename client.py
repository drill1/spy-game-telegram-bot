import random 
import math

class room:    
    
    

    def __init__(self, seed, playerNum):
        self.key = seed
        self.playerNum=playerNum
        self.spyNum=math.floor(playerNum/3)
        self.gamersNum=playerNum-self.spyNum
        self.location=""
        self.roles=[]
        self.playerIds = []
        self.generateRoles()

    def generateRoles(self):
        locationList = []       #список текущей локации-ролей
        locationLTable = []     #список всех локаций из файла
        DataFile = "res/roles.txt"
        with open(DataFile, 'r', encoding='utf-8') as file:
            locationLTable = file.read().splitlines()   #прочитать все локации из файла

        rolesNum = 0
        while rolesNum < self.playerNum - self.spyNum:          #поиск локации с количеством ролей >=введенное количество мирных игроков
            locationList = locationLTable[random.randint(0, len(locationLTable) - 1)]       #прочитать локацию
            locationList1 = locationList.split('|')
            rolesNum = int(locationList1.pop(0))      #первый элемент в локации - число мирных ролей в ней
            locationList1.pop(0)

        locationLTable.clear()      #удалить лишние локации
            
        locationList = locationList.split("|")  #разделить остальную строку на список
        locationList.pop(0)
        self.location = locationList.pop(0) #первый элемент списка - название самой локации, остальные - назания ролей
        print("Ваша локация: "+str(self.location))
        
        self.roles = locationList   #заполнить список ролей ролями со списка
        while len(self.roles) > self.playerNum - self.spyNum:     #удалить лишние+случайные роли из списка ролей
            self.roles.pop(random.randint(0, len(self.roles)-1))
		
        for i in range(0, self.spyNum):   #добавление шпионов по количеству 
            self.roles.insert(random.randint(0, len(self.roles)-1), "Шпион")
            

    

    def get_role(self):
        if self.playerNum > 0:
            self.playerNum = self.playerNum - 1 
            return self.roles.pop()
        else:
            return None
            