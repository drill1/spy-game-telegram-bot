import sys  #подключениие необходимых библиотек
from client import room
import telebot
import random 
import requests
import urllib3
import chardet
import certifi
import idna
import mytoken	#в .gitignore
import threading
from threading import Timer
import time
from time import sleep
from telebot import types
import re


bot = mytoken.get_bot()


class stat:     #статический список комнат
    rooms=[]
	
waitForSpyNum = False

@bot.message_handler(content_types=['text'])    #обработчик ввходящих текстовых сообщений
def handle_text_messages(message):
    markup = types.ReplyKeyboardMarkup(row_width=2)     #первоначальная расстановка кнопок
    createRoomBtn = types.KeyboardButton('🕵️ Создать комнату')
    connectBtn = types.KeyboardButton('🍻 Подключиться')
    helpBtn = types.KeyboardButton('❓ Помощь')
    markup.add(createRoomBtn, connectBtn, helpBtn)    
    num=-1  #временная переменная для числа игроков и/или номера комнаты
    try:
        num = int(message.text)
    except Exception:
        num=-1

    if message.text[0] == 'R':      #подключение по ID комнаты
        corr = False

        for i in stat.rooms:
            if i.key==message.text:
                if message.from_user.id in i.playerIds:
                    bot.send_message(message.from_user.id, "Вы уже получили свою роль!")
                    corr = True
                    break
                else:
                    role = i.get_role()
                    if "Шпион" in role:
                        bot.send_message(message.from_user.id, "Ваша роль - " + role) 
                        
                    else:
                        bot.send_message(message.from_user.id, "Ваша локация - " + i.location + "\nВаша роль - " + role) 
                    i.playerIds.append(message.from_user.id)
                    if i.playerNum==0:
                        stat.rooms.remove(i)
                    corr = True
                    break
        if corr == False:
            bot.send_message(message.from_user.id, "Такой комнаты нет, уточните ID у хоста") 


    elif num>=3 and num<13:      #ввод количества игроков
        
        corr=False  #уникальность ключа
        while corr==False:  #пока есть такой же ключ
            corr=True
            key = "R"+str(random.randint(100, 999)) #сгенерировать новый ключ
            for i in stat.rooms:    #проверить, есть ли он в списке
                if i.key==key:
                    corr=False 
        this_room = room(key, num)  #текущая комната
        stat.rooms.insert(0, this_room)	    #добавить ее в список


        x = threading.Thread(target=addThread, args=(this_room,))   #создать поток, который удалить комнату через сколько-то(600) секунд

        x.start()   #запустить поток


        bot.send_message(message.from_user.id, "ID Вашей комнаты - " + str(this_room.key) + "\n" 
                                                    +  "Шпионов в комнате - " + str(this_room.spyNum))

    elif "Создать комнату" in message.text: 

        bot.send_message(message.from_user.id, "Сколько вас будет играть?")

    elif "Подключиться"  in message.text:
        
        bot.send_message(message.from_user.id, "Введите трехзначный код, начинающийся с буквы 'R'")
            
    elif "Помощь"  in message.text or message.text == "/help":

        bot.send_message(message.from_user.id,  "/rules - правила игры \n"+
                                                "/questions - примеры вопросов игрокам \n"+
                                                "/feedback - связь с разработчиками \n"+
                                                "/docs - документация по проекту \n"+
                                                "/license - лицензия продукта \n"+
                                                "/buttons - вернуть пропавшие кнопки")
    
    elif message.text == "/docs":

        bot.send_message(message.from_user.id, 'Открыть [веб-страницу с документацией](https://gitlab.com/drill1/spy-game-telegram-bot/-/wikis/%D0%93%D0%BB%D0%B0%D0%B2%D0%BD%D0%B0%D1%8F).', parse_mode='Markdown')
		
    elif message.text == "/questions":
        
        DataFile = "res/questions.txt"
        with open(DataFile, 'r', encoding='utf-8') as file:
            questions = file.read()
            bot.send_message(message.from_user.id,  questions) 
        bot.send_message(message.from_user.id,  "\n")
												
    elif message.text == "/rules":
        
        DataFile = "res/rules.txt"
        with open(DataFile, 'r', encoding='utf-8') as file:
            rules = file.read()
            bot.send_message(message.from_user.id,  rules) 

    elif message.text == "/feedback":
        bot.send_message(message.from_user.id,  "Связь с разработчиками: mailto:ist_75@mail.ru")
		
    elif message.text == "/license":

        bot.send_message(message.from_user.id,  "[Файл лицензии](https://gitlab.com/drill1/spy-game-telegram-bot/-/blob/master/LICENSE)", parse_mode='Markdown')

    elif message.text == "/start":
        bot.send_message(message.from_user.id,  "Добро пожаловать в эмулятор настольной игры 'Шпион'",reply_markup=markup)
		
    elif message.text == "/buttons":
        bot.send_message(message.from_user.id,  "Больше так не делай. Если не помогло - нажми на специальную кнопку справа от поля ввода сообщений",reply_markup=markup)
    
    
    
    else:
        bot.send_message(message.from_user.id, "Неизвестная команда")
            
        
def addThread(room):    #запускать в потоке!
    sleep(600)   #ждать сколько-то секунд
    stat.rooms.remove(room) #удалить комнату


bot.polling(none_stop=True, interval=0)
